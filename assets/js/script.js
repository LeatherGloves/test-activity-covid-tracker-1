let name = prompt("Welcome to CovidTrack! Please input your name:")
const doyouhavecovid2 = () => {
	let question1 = confirm("Have you traveled in the past 14 days to countries with local transmission and risk of importation? OK for Yes Cancel for No")
	if (question1 == true){
		let question2a = confirm("Do you have fever(>37C) AND/OR Respiratory Illness(cough and/or colds) OK for Yes Cancel for No")
		if (question2a == true){
			document.getElementById("result").innerHTML = `${name}, you are under classification: Patients Under Investigation (PUI)`
			document.getElementById("description").innerHTML = `If with mild manifestations, refer to L2/L3 hospital. If with severe manifestations, refer to Referral Hospitals`
		} else {
			document.getElementById("result").innerHTML = `${name}, you are under classification: Person Under Monitoring (PUM)`
			document.getElementById("description").innerHTML = `Home Quarantine`
		} 
	} else {
		let question2b = confirm("Do you have any History of Exposure? (e.g. providing direct care for COVID-19 patient, working together or staying in the same close environment of a COVID-19 carrier, traveling together with COVID-19 patient in any kind of conveyance, or living in the same household as a COVID-19 patient within a 14-day period) OK for Yes Cancel for No")
		if (question2b == true) {
			let question2a = confirm("Do you have fever(>37C) AND/OR Respiratory Illness(cough and/or colds) OK for Yes Cancel for No")
			if (question2a == true){
				document.getElementById("result").innerHTML = `${name}, you are under classification: Patients Under Investigation (PUI)`
				document.getElementById("description").innerHTML = `If with mild manifestations, refer to L2/L3 hospital. If with severe manifestations, refer to Referral Hospitals`
			} else {
				document.getElementById("result").innerHTML = `${name}, you are under classification: Person Under Monitoring (PUM)`
				document.getElementById("description").innerHTML = `Home Quarantine`
			}
		} else {
			document.getElementById("result").innerHTML = `${name}, you are neither Patient Under Investigation (PUI) nor Person Under Monitoring (PUM)`
			document.getElementById("description").innerHTML = `Manage Appropriately`
		}
	}
}

document.getElementById('start').onclick = () => {
	document.getElementById('startbutton').innerHTML = null
	setTimeout(()=>{
		doyouhavecovid2()
	}, 800)
}